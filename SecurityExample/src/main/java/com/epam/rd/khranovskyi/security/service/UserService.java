package com.epam.rd.khranovskyi.security.service;

import com.epam.rd.khranovskyi.security.entity.User;

public interface UserService {
    void save(User user);

    User findByUserName(String name);
}
