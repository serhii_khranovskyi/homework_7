package com.epam.rd.khranovskyi.security.service.impl;

import com.epam.rd.khranovskyi.security.entity.Role;
import com.epam.rd.khranovskyi.security.entity.User;
import com.epam.rd.khranovskyi.security.repository.RoleRepository;
import com.epam.rd.khranovskyi.security.repository.UserRepository;
import com.epam.rd.khranovskyi.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        System.out.println("User password-->" + user.getPassword());
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.getOne(2L));
        user.setRoles(roles);
        userRepository.save(user);
    }

    @Override
    public User findByUserName(String name) {
        return userRepository.findByUsername(name);
    }
}
