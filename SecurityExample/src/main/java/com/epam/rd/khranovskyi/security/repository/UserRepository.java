package com.epam.rd.khranovskyi.security.repository;

import com.epam.rd.khranovskyi.security.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String name);
}
