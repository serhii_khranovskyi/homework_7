package com.epam.rd.khranovskyi.security.repository;

import com.epam.rd.khranovskyi.security.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
