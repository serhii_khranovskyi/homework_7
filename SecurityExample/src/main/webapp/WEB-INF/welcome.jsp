<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Welcome</title>
</head>
<body>


<div class="container">
    <div style="text-align: center">
        <p>Hello, it`s a welcome page.Please, sign in. </p>
        <h4 class="text-center"><a href="${contextPath}/login">Sign in</a></h4>
    </div>
</div>

<c:if test="${pageContext.request.isUserInRole('ROLE_ADMIN')}">
    <h4 class="text-center"><a href="${contextPath}/admin">Go to admin page</a></h4>
</c:if>

<c:if test="${pageContext.request.isUserInRole('ROLE_USER')}">
    <h4 class="text-center"><a href="${contextPath}/user">Go to user page</a></h4>
</c:if>

</body>
</html>