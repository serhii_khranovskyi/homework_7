<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Admin</title>
</head>

<body>
<div class="container">
    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="post" action="${contextPath}/logout">
        </form>

        <h2>Admin Page ${pageContext.request.userPrincipal.name}
        </h2>
        <a onclick="document.forms['logoutForm'].submit()">Logout</a>
    </c:if>
</div>

</body>
</html>