<html>
<c:set var="title" value="Registration"/>

<body>
<div class="container">
    <div class="row main-form">
        <form id="registration_form" action="/registration" method="post">
            <input type="hidden" name="command" value="registration"/>
            <br/> <br/>
            <p>Enter your name</p>
            <input type="text" class="form-control" name="username" id="name"/>
            <br/> <br/>
            <p>Enter your password</p>
            <input type="password" class="form-control" name="password" id="password"/>
            <br/> <br/>
                <input type="submit" id="button" class="btn btn-primary btn-lg btn-block login-button"
                       value="Apply">
        </form>
    </div>
</div>
</body>
</html>
